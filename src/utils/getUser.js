import axios from "axios";

const getUser = async (id) => {
  const user = await axios("https://fsw-teamb-backend.herokuapp.com/profile/"+id);

  return user;
};

export default getUser;
