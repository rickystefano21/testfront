import axios from "axios";

const getAllUser = async () => {
  const user = await axios("https://fsw-teamb-backend.herokuapp.com/datauser");

  return user;
};

export default getAllUser;
